﻿##Demo
- Link: http://i392329.hera.fhict.nl/mercedes-event/

##Description
This was the first major group project I had, which lasted for a full semester. It marked the end of my first year at university - the "Propaedeutic phase". The project goal was to develop a digital system, with numerouse requirements, to handle a fictitious "Mercedes-Benz" car event. the end product included fully functional website, which would be used for visitors to get informed, buy tickets and book accommodations and a C# application, which will be used for the purposes of Check In/Out system, vistiros to buy products, and get data overview. My job in the project was to develop the website.

##Technologies

- HTML
- CSS
- JS
- PHP
- MYSQL
- C#

##Recommendations for use
One of the requirements for the website were to have users with different access to website functionalities. For this reason I reccommend that you use the following credentials to use the website as a normal visitor:

- Username: user
- Password: user

and the following creadentials for the admin of website, who can alter website information:

- Username: admin
- Password: admin